package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class RegistrationActivity extends AppCompatActivity {
    EditText login,password,repeatPassword,email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        login = findViewById(R.id.regLoginEditText);
        password = findViewById(R.id.regPasswordEditText);
        repeatPassword = findViewById(R.id.repeatPasswordEditText);
        email = findViewById(R.id.emailEditText);
    }

    public void regClick(View view) {
        String mylogin = login.getText().toString();
        String mypassword = password.getText().toString();
        String myrepeatpassword = repeatPassword.getText().toString();
        String myemail = email.getText().toString();
        if(!mylogin.equals("") && !mypassword.equals("") && !myrepeatpassword.equals("") && !myemail.equals("")){
            if(mylogin.equals("admin") && mypassword.equals("admin") && myrepeatpassword.equals("admin")){
                if(myemail.indexOf("@")>0){
                    Intent intent = new Intent(RegistrationActivity.this,MenuActivity.class);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(this,"Email имеет неверный формат",Toast.LENGTH_LONG).show();
                }
            }
            else {
                Toast.makeText(this,"Логин или пароль неверны",Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(this,"Поля не должны быть пустыми",Toast.LENGTH_LONG).show();
        }




    }

    public void cancelClick(View view) {

        Intent intent = new Intent(RegistrationActivity.this,AuthorizationActivity.class);
        startActivity(intent);
    }
}